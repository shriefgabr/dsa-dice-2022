package oy.tol.tra;

public class QueueImplementation<E> implements QueueInterface<E> {

   private Object[] itemArray;
   private int capacity = 10;
   private int head = 0;
   private int tail = 0;
   private int size = 0;

    // * Allocates a queue with a default capacity.
   public QueueImplementation() throws QueueAllocationException {
      try {
         itemArray = new Object[capacity];
      } catch (QueueAllocationException e) {
         throw new QueueAllocationException("Queue allocation failed.");
      }
   }

   public QueueImplementation(int capacity) throws QueueAllocationException {
      if (capacity < 2)
         throw new QueueAllocationException("Queue allocation failed.");

      itemArray = new Object[capacity];
      this.capacity = capacity;
   }

   @Override
   public int capacity() {
      return capacity;
   }

   @Override
   public void enqueue(E element) throws QueueAllocationException, NullPointerException {

      if (element == null)
         throw new NullPointerException("Cannot add null element to queue.");

      if (size == capacity) {
         try {
            Object[] tmp = new Object[capacity * 2];
            int current = head;
            for (int i = 0; i < size(); i++) {
               tmp[i] = itemArray[current];
               current = (current + 1) % capacity;
            }
            itemArray = tmp;
            head = 0;
            tail = size();
            capacity = capacity * 2;
         } catch (QueueAllocationException e) {
            throw new QueueAllocationException("Queue allocation failed.");
         }
      }

      if (tail >= capacity && head > 0)
         tail = 0;

      itemArray[tail++] = element;
      size++;
   }

   @SuppressWarnings("unchecked")
   @Override
   public E dequeue() throws QueueIsEmptyException {
      if (size() == 0)
         throw new QueueIsEmptyException("Queue is empty. No elements to pop");

      E element = (E) itemArray[head];
      head++;
      size--;

      head = head % capacity;
      return element;
   }

   @SuppressWarnings("unchecked")
   @Override
   public E element() throws QueueIsEmptyException {
      if (size == 0)
         throw new QueueIsEmptyException("Queue is empty. Nothing to retrieve.");
      return (E) itemArray[head];
   }

   @Override
   public int size() {
      return size;
   }

   @Override
   public void clear() {
      for (int i = 0; i < head; i++)
         itemArray[i] = null;
      head = 0;
      tail = 0;
      size = 0;
   }

   @Override
   public boolean isEmpty() {

      return size == 0;
   }

   @Override
   public String toString() {
      if (size == 0)
         return "[]";

      String retStr = "[" + itemArray[head];
      int current = head;
      current = (current + 1) % capacity;
      for (int i = 0; i < size() - 1; i++) {

         retStr += ", " + itemArray[current];
         current = (current + 1) % capacity;
      }
      retStr += "]";

      return retStr;
   }
}
