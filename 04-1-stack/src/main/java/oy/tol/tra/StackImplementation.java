package oy.tol.tra;

public class StackImplementation<E> implements StackInterface<E> {

   private Object [] itemArray;
   private int capacity = 10;
   private int currentIndex = -1;
   
   public StackImplementation() throws StackAllocationException {
      itemArray = new Object[capacity];
   }

    public StackImplementation(int capacity) throws StackAllocationException {
      if(capacity < 2) throw new StackAllocationException("Stack Allocation");
      itemArray = new Object[capacity];
      this.capacity = capacity;
   }

   @Override
   public int capacity() {
      return capacity;
   }

   @Override
   public void push(E element) throws StackAllocationException, NullPointerException {
      if(element == null) throw new NullPointerException("Null element can not be inserted");
      if(currentIndex + 1 >= capacity)
      {
      Object[] newArray = new Object[itemArray.length + 1];
      System.arraycopy(itemArray, 0, newArray, 0, itemArray.length);

      itemArray = newArray;
      itemArray[++currentIndex] = element;
      ++capacity;
      }  
      
      else itemArray[++currentIndex] = element;
   }

   @SuppressWarnings("unchecked")
   @Override
   public E pop() throws StackIsEmptyException {
      if(currentIndex == -1) throw new StackIsEmptyException("Stack is empty. No elements to pop");

      return (E) itemArray[currentIndex--];
   }

   @SuppressWarnings("unchecked")
   @Override
   public E peek() throws StackIsEmptyException {
      if(currentIndex == -1) throw new StackIsEmptyException("Stack is empty. Nothing to see.");
      return (E) itemArray[currentIndex];
   }

   @Override
   public int size() {
      return currentIndex+1;
   }

   @Override
   public void clear() {
      for(int i = 0; i < currentIndex; i++) itemArray[i] = null;
      currentIndex = -1;
   }

   @Override
   public boolean isEmpty() {

      return currentIndex == -1;
   }

   @Override
   public String toString() {
      if(currentIndex == -1) return "[]";
      String retStr = "[" + itemArray[0]; 
      for(int i = 1; i <= currentIndex; i++){
         retStr += ", " + itemArray[i];
      }
      retStr += "]";
      
      return retStr;
   }
}
