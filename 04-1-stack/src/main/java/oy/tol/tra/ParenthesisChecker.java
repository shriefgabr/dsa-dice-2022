package oy.tol.tra;


public class ParenthesisChecker {

   private ParenthesisChecker() {
   }
   // checks if the given string has matching opening and closing parentheses
   public static int checkParentheses(StackInterface<Character> stack, String fromString) throws ParenthesesException {

      int numOfParentheses = 0;
      for (int i = 0; i < fromString.length(); i++) {
         if (fromString.charAt(i) == '(' || fromString.charAt(i) == '[' || fromString.charAt(i) == '{') {
            try{
               stack.push(fromString.charAt(i));
            }
            catch(StackAllocationException e){
               throw new StackAllocationException(fromString);
            }
         } else if (fromString.charAt(i) == ')' || fromString.charAt(i) == ']' || fromString.charAt(i) == '}') {
            if (stack.isEmpty())
               throw new ParenthesesException("Too many closing parentheses", -1);
            else if (fromString.charAt(i) == ')' && stack.peek() != '(')
               throw new ParenthesesException("Incompatible closing/opening parenthesis", -3);
            else if (fromString.charAt(i) == ']' && stack.peek() != '[')
               throw new ParenthesesException("Incompatible closing/opening parenthesis", -3);
            else if (fromString.charAt(i) == '}' && stack.peek() != '{')
               throw new ParenthesesException("Incompatible closing/opening parenthesis", -3);
            else {
               stack.pop();
               numOfParentheses++;
            }
         }
      }
      if(!stack.isEmpty()) throw new ParenthesesException("Too few closing parentheses", -2);
      return numOfParentheses*2;
   }
   
}
