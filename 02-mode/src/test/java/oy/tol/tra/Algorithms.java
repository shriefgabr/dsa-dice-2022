package oy.tol.tra;

public class Algorithms {
    public static class ModeSearchResult<T> {
        public T theMode;
        public int count = 0;
    }

    public static <T extends Comparable<T>> ModeSearchResult<T> findMode(T[] array) {
        ModeSearchResult<T> result = new ModeSearchResult<>();
        if (array == null || array.length < 2) {
            result.theMode = null;
            result.count = -1;
            return result;
        }

        if (array.length == 2 && (array[0] == array[1])) {
            result.theMode = array[0];
            result.count = 2;
            return result;
        }
        int localCount = 0;
        for (int i = 0; i < array.length; ++i) {

            
            for (int j = i; j < array.length; ++j) {
                if (array[i].compareTo(array[j]) == 0) {
                    localCount++;
                }
            }

            if (localCount > result.count) {
                result.count = localCount;
                result.theMode = array[i];
            }
            localCount = 0;
        }

        return result;
    }

    public static <T> void swap(T[] array, int first, int second) {

        T tmp = array[second];
        array[second] = array[first];
        array[first] = tmp;
    }

    public static <T extends Comparable<T>> void sort(T[] array) {
        // implementation here
        int n = 1;
        while (n < array.length) {
            int i = array.length - 1;
            while (i > 0) {
                if (array[i].compareTo(array[i - 1]) < 0) {

                    // T tmp = array[i];
                    // array[i] = array[i - 1];
                    // array[i - 1] = tmp;
                    swap(array, i - 1, i);
                }
                i--;
            }
            n++;
        }
    }

    // ...
    public static <T> void reverse(T[] array) {
        // toteutus tähän...
        int i = 0;
        while (i < array.length / 2) {

            swap(array, i, array.length - i - 1);
            i++;
        }
    }

}
