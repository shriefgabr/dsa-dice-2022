# Report
Based on the code itself, the time complexity is O(n<sup>2</sup>) since it consists of a nested for loop. The memory complexity is O(1) since it just keeps track of the `localCount` variable and updates the `result.TheMode` and `result.count` based off of that.

## Graphs
### Strings
![Performance - Strings](Strings.png)
### Doubles
![Performance - Doubles](Doubles.png)
>**Comment** The time grows exponentially with the number of elements. This is expected since I used nested for loop making the time complexity O(n^2).\
>**Observation** Doubles were generally faster to process and find the mode in than Strings. 