package oy.tol.tra;

public class Algorithms {
    public static <T extends Comparable<T>> void sort(T[] array) {
        int n = 1;
        while (n < array.length) {
            int i = array.length - 1;
            while (i > 0) {
                if (array[i].compareTo(array[i - 1]) < 0) {
                    swap(array, i, i - 1);
                }
                i--;
            }
            n++;
        }
    }

    public static <T> void swap(T[] array, int first, int second) {

        T tmp = array[second];
        array[second] = array[first];
        array[first] = tmp;
    }

    public static <T> void reverse(T[] array) {
        int i = 0;
        while (i < array.length / 2) {
            swap(array, i, array.length - i - 1);
            i++;
        }
    }

}
