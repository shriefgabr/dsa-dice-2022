package oy.tol.tra;

public class KeyValueHashTable<K extends Comparable<K>, V> implements Dictionary<K, V> {

    private Pair<K, V>[] table = null;
    private int count = 0;
    private int collisionCounter = 0;
    static double FILL_FACTOR = 0.80;
    private int reallocationCounter = 0;
    private int probeCounter = 0;
    private int maxProbeCount = 0;


    public KeyValueHashTable(int capacity) throws OutOfMemoryError {
        try {
            ensureCapacity(capacity);
        } catch (OutOfMemoryError e) {
            throw new OutOfMemoryError("Out of Memory error");
        }
    }

    public KeyValueHashTable() throws OutOfMemoryError {
        try {
            ensureCapacity(20);
        } catch (OutOfMemoryError e) {
            throw new OutOfMemoryError("Out of Memory error");
        }
    }

    @Override
    public Type getType() {
        return Type.HASHTABLE;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void ensureCapacity(int size) throws OutOfMemoryError {
        if (size < 20) {
            size = 20;
        }

        try {
            table = (Pair<K, V>[]) new Pair[size];
        } catch (OutOfMemoryError e) {
            throw new OutOfMemoryError("Out of Memory error");

        }

    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public String getStatus() {

        String toReturn = String.format(
                "Hash table fill factor is %f\nHash table had %d collisions when filling the hash table.\nHash table had to reallocate %d times. Everytime doubling its size.\nHash table had to probe %d times in the worst case.\nHash table current fill rate is %.2f%%%n",
                FILL_FACTOR, collisionCounter, reallocationCounter, maxProbeCount, (count / (double) table.length) * 100.0);

        return toReturn;
    }

    @Override
    public boolean add(K key, V value) throws IllegalArgumentException, OutOfMemoryError {
        if (null == key || value == null)
            throw new IllegalArgumentException("Neither key nor value can be null");
        Pair<K, V> p = new Pair<K, V>(key, value);
        if ((1.0 * count) / (1.0 * table.length) >= FILL_FACTOR) {
            try {
                reallocate();
            } catch (OutOfMemoryError e) {
                throw new OutOfMemoryError("Out of Memory error");
            }
        }
        int hashValue = key.hashCode();
        int index = (hashValue & 0x7fffffff) % table.length;

        if (table[index] == null) {
            table[index] = p;
            count++;
        } else {
            Boolean cannotProbe = true;
            collisionCounter++;
            probeCounter = 0;
            while (cannotProbe) {
                probeCounter++;
                index = (index + 1) % table.length;
                if (table[index] == null) {
                    cannotProbe = false;
                    table[index] = p;
                    count++;
                }
                maxProbeCount = Math.max(probeCounter, maxProbeCount);
            }
        }

        return true;
    }

    @Override
    public V find(K key) throws IllegalArgumentException {
        if (null == key)
            throw new IllegalArgumentException("Not key nor value can be null");
        int hashValue = key.hashCode();
        int index = (hashValue & 0x7fffffff) % table.length;

        while (table[index] != null) {
            if (table[index].getKey().equals(key))
                return table[index].getValue();
            index = (index + 1) % table.length;
        }

        return null;
    }

    @Override
    @java.lang.SuppressWarnings({ "unchecked" })
    public Pair<K, V>[] toSortedArray() {
        Pair<K, V>[] sorted = (Pair<K, V>[]) new Pair[count];
        int newIndex = 0;
        for (int i = 0; i < table.length; i++) {
            if (table[i] != null) {

                sorted[newIndex++] = table[i];

            }
        }
        Algorithms.fastSort(sorted, 0, sorted.length - 1);
        return sorted;
    }

    @Override
    public void compress() throws OutOfMemoryError {

        Algorithms.partitionByRule(table, table.length, element -> element == null);
    }

    public void reallocate() throws OutOfMemoryError {
        try {
            Pair<K, V>[] temp = new Pair[table.length * 2];

            collisionCounter = 0;
            probeCounter = 0;
            for (int i = 0; i < table.length; i++) {
                if (table[i] != null) {

                    K key = table[i].getKey();
                    V value = table[i].getValue();

                    int hashValue = key.hashCode();
                    int index = (hashValue & 0x7fffffff) % temp.length;
                    Pair<K, V> p = new Pair<K, V>(key, value);

                    if (temp[index] == null) {
                        temp[index] = p;
                    } else {
                        Boolean cannotProbe = true;
                        collisionCounter++;
                        probeCounter = 0;
                        while (cannotProbe) {
                            probeCounter++;
                            index = (index + 1) % temp.length;
                            if (temp[index] == null) {
                                cannotProbe = false;
                                temp[index] = p;
                            }
                        }
                        maxProbeCount = Math.max(probeCounter, maxProbeCount);
                    }

                }
            }
            table = temp;
            reallocationCounter++;
        } catch (OutOfMemoryError e) {
            throw new OutOfMemoryError("Out of Memory error");
        }
    }
    
}
