package oy.tol.tra;

public class Person implements Comparable<Person> {
    private String firstName;
    private String lastName;

    public Person(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getFullName() {
        return lastName + " " + firstName;
    }

    @Override
    public int compareTo(Person person2) {
        return getFullName().compareTo(person2.getFullName());
    }

    @Override
    public boolean equals(Object person2) {
        if (person2 instanceof Person) {
            return getFullName().equals(((Person)person2).getFullName());
        }
        return false;
    }

    @Override
    public int hashCode() {
        
        int result = 1;
        
        int prime = 31;
        for(int i = 0; i < getFullName().length(); i++){
            result = prime*result + getFullName().charAt(i);
        }
        // Alternate hashing using bit shifting 
        // for(int i = 0; i < getFullName().length(); i++){
        //     result = (result << 5) - result + getFullName().charAt(i);
        // }
        
        return result;
    }

}
