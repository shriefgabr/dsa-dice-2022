package oy.tol.tra;

import java.util.function.Predicate;

public class Algorithms {

    public static <T extends Comparable<T>> int binarySearch(T aValue, T[] fromArray, int fromIndex, int toIndex) {
        if (aValue.compareTo(fromArray[toIndex]) > 0 || aValue.compareTo(fromArray[fromIndex]) < 0)
            return -1;
        int l = fromIndex;
        int r = toIndex;

        while (l <= r) {

            int mid = l + (r - l) / 2;

            if (fromArray[mid].compareTo(aValue) == 0)
                return mid;
            else if (aValue.compareTo(fromArray[mid]) > 0) {
                l = mid + 1;
            } else if (aValue.compareTo(fromArray[mid]) < 0) {

                r = mid - 1;
            }
        }

        return -1;
    }

    public static <T> int partitionByRule(T[] array, int count, Predicate<T> rule) {

        int nextIndex = 0;
        int index = count;
        for (int i = 0; i < count; i++) {
            if (rule.test(array[i])) {
                index = i;
                break;
            }
        }

        if (index >= count) {
            return count;
        }

        nextIndex = index + 1;

        while (nextIndex < count) {
            if (!rule.test(array[nextIndex])) {
                swap(array, index, nextIndex);
                index = index + 1;
            }
            nextIndex++;
        }
        return index;

    }

    public static <T> void swap(T[] array, int first, int second) {

        T tmp = array[second];
        array[second] = array[first];
        array[first] = tmp;
    }

    public static class ModeSearchResult<T> {
        public T theMode;
        public int count = 0;
    }

    public static <T extends Comparable<T>> ModeSearchResult<T> findMode(T[] array) {
        ModeSearchResult<T> result = new ModeSearchResult<>();
        if (array == null) {
            result.theMode = null;
            result.count = -1;
            return result;
        }

        if (array.length < 2) {
            result.theMode = null;
            result.count = -1;
            return result;
        }

        if (array.length == 2 && (array[0] == array[1])) {
            result.theMode = array[0];
            result.count = 2;
            return result;
        }

        for (int i = 0; i < array.length; i++) {

            int localCount = 0;
            for (int j = 0; j < array.length; j++) {
                if (array[i].compareTo(array[j]) == 0) {
                    localCount++;
                }
            }

            if (localCount > result.count) {
                result.count = localCount;
                result.theMode = array[i];
            }
        }

        return result;
    }

    public static <T extends Comparable<T>> void sort(T[] array) {
        int n = 1;
        while (n < array.length) {
            int i = array.length - 1;
            while (i > 0) {
                if (array[i].compareTo(array[i - 1]) < 0) {
                    swap(array, i - 1, i);
                }
                i--;
            }
            n++;
        }
    }
    public static <T extends Comparable<T>> void fastSort(T[] array, int low, int high) {
        // Implementing Quick Sort
        
        if (low < high) {
            int partitionIndex = partition(array, low, high);
            fastSort(array, low, partitionIndex - 1);
            fastSort(array, partitionIndex + 1, high);
        }
    }

    public static <T extends Comparable<T>> int partition(T[] array, int low, int high) {
        T pivot = array[high];
        int i = low - 1;
        for (int j = low; j <= high-1; j++) {
            if (array[j].compareTo(pivot) <= 0) {
                i++;
                swap(array, i, j);
            }
        }
        
        swap(array, i + 1, high);
        return i + 1;
    }
    public static <T> void reverse(T[] array) {
        int i = 0;
        while (i < array.length / 2) {

            swap(array, i, array.length - i - 1);
            i++;
        }
    }

}
