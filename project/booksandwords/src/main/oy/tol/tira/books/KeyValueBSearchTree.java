package oy.tol.tira.books;

public class KeyValueBSearchTree<K extends Comparable<K>, V> implements Dictionary<K, V> {

    // This is the BST implementation, KeyValueHashTable has the hash table
    // implementation
    TreeNode<K,V> root;
    private int count = 0;
    public int maxDepth = Integer.MIN_VALUE;
    @Override
    public Type getType() {
        return Type.BST;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public String getStatus() {
        String toReturn = String.format("Tree has max depth %d\n",
                maxDepth);
        return toReturn;
    }

    @Override
    public boolean add(K key, V value) throws IllegalArgumentException, OutOfMemoryError {
        if (null == key || null == value ) throw new IllegalArgumentException("Key cannot be null");
        try{
        TreeNode<K,V> node = new TreeNode<>();
        node.key = key;
        node.value = value;
        int depth = 0;

        if (root == null)
            root = node;
        else {
            depth = root.add(node, depth);
        }
    
        count++;
        maxDepth = Math.max(maxDepth, depth);
        return false;}
        catch(OutOfMemoryError e)
        {
            throw new OutOfMemoryError("Out of memory. Cannot add new node.");
        }
    }

    @Override
    public V find(K key) throws IllegalArgumentException {
        if (null == key ) throw new IllegalArgumentException("Key cannot be null");
        TreeNode<K,V> node = root;

        while (node != null && !node.key.equals(key))
        {
        if(node.key.compareTo(key) < 0)
            node = node.right;
        else
            node = node.left;
        }
        if(node == null) return null;
        return (V) node.value;
    }

    @Override
    public void ensureCapacity(int size) throws OutOfMemoryError {
    }

    @Override
    public Pair<K, V>[] toSortedArray() {
        Pair<K, V>[] arr = (Pair<K,V>[]) new Pair[count];
        TreeNode<K,V> node = new TreeNode<K,V>();
        node = root;
        bstInOrder(node, arr, 0);
        return arr;
    }

    @Override
    public void compress() throws OutOfMemoryError {
    }
    
    public int bstInOrder(TreeNode<K,V> node, Pair<K,V>[] arr, int index) {

        if(node == null) return index;
        index = bstInOrder(node.left, arr, index);
        Pair<K,V> p = new Pair<K,V>(node.key, node.value);
        arr[index] = p;
        index++;
        index = bstInOrder(node.right, arr, index );
        
        return index;
    }

    public TreeNode<K,V> findKey(K key) throws IllegalArgumentException {
        if (null == key ) throw new IllegalArgumentException("Key cannot be null");
        TreeNode<K,V> node = root;

        while (node != null && !node.key.equals(key))
        {
        if(node.key.compareTo(key) < 0)
            node = node.right;
        else
            node = node.left;
        }
        if(node == null) return null;
        return node;
    }

}
