package oy.tol.tira.books;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.FileReader;
import java.io.BufferedReader;
import java.nio.charset.StandardCharsets;

public class MyBookImplementation implements Book {

    BufferedReader bookReader;
    BufferedReader ignoreWordsReader;
    KeyValueHashTable<String, Boolean> ignoreWordsTable = new KeyValueHashTable<String, Boolean>();
    KeyValueBSearchTree<String, Integer> uniqueWordsTree = new KeyValueBSearchTree<String, Integer>();

    int uniqueWordCount = 0;
    int totalWordCount = 0;
    boolean top100Generated = false;
    Pair<String, Integer>[] top100;

    @Override
    public void setSource(String fileName, String ignoreWordsFile) throws FileNotFoundException {

        try {
            bookReader = new BufferedReader(new FileReader(fileName, StandardCharsets.UTF_8));
        } catch (IOException e) {
            throw new FileNotFoundException("Source file not found");
        }
        try {
            ignoreWordsReader = new BufferedReader(new FileReader(ignoreWordsFile, StandardCharsets.UTF_8));
        } catch (IOException e) {
            throw new FileNotFoundException("ignoreWords file not found");
        }

        String line;
        try {
            while ((line = ignoreWordsReader.readLine()) != null) {
                String words[] = line.split(",");

                for (String word : words) {
                    word = word.toLowerCase();
                    ignoreWordsTable.add(word, true);
                }

            }
        } catch (IOException e) {
            throw new FileNotFoundException("ignoreWords file not found");
        }
    }

    @Override
    public void countUniqueWords() throws IOException, OutOfMemoryError {
        String line;
        while ((line = bookReader.readLine()) != null) {
            if (line.length() > 0) {

                String words[] = line.split("[^a-zA-Za-яA-Я0-9]+");

                for (String currWord : words) {
                    if (currWord.length() == 1)
                    continue;

                    currWord = currWord.toLowerCase();
                    String word = "";
                    for (int i = 0; i < currWord.length(); i++) {

                        if (Character.isLetter(currWord.charAt(i))) {
                            word += currWord.charAt(i);
                        }
                    }

                    if (ignoreWordsTable.find(word) == null && word.length() > 1) {
                        totalWordCount++;

                        TreeNode<String, Integer> node = uniqueWordsTree.findKey(word);
                        Integer wordCount = null;
                        if (node != null)
                            wordCount = node.value;

                        if (wordCount == null) {

                            uniqueWordsTree.add(word, 1);
                            uniqueWordCount++;
                        } else if (wordCount >= 0) {
                            {
                                node.value = ++wordCount;
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public void report() {
        if(!top100Generated) getTop100();

        for(int i = 0; i < top100.length; i++){
            System.out.println(top100[0]);
        }
        
        System.out.println("- Total word count: " + totalWordCount); 
        System.out.println("- Unique word count: " + uniqueWordCount);
        System.out.println("- Unique words tree max depth: " + uniqueWordsTree.maxDepth);
        System.out.println("- Ignored word count: " + ignoreWordsTable.size());
        System.out.println("- Ignored words table: \n" + ignoreWordsTable.getStatus());
        
    }

    @Override
    public void close() {

        if (bookReader != null) {
            try {
                bookReader.close();
            } catch (IOException e) {
                System.out.println("Could not close words readers.");
            }
        }
        if (ignoreWordsReader != null) {
            try {
                ignoreWordsReader.close();
            } catch (IOException e) {
                System.out.println("Could not close ignore words readers.");
            }
        }
    }

    @Override
    public int getUniqueWordCount() {
        return uniqueWordCount;
    }

    @Override
    public int getTotalWordCount() {
        return totalWordCount;
    }

    @Override
    public String getWordInListAt(int position) {
        if (uniqueWordCount == 0 || position > uniqueWordCount || position < 0)
            return null;


        if(!top100Generated) getTop100();
        return top100[position].getKey();
    }

    @Override
    public int getWordCountInListAt(int position) {
        if (position < 0 || position > uniqueWordCount || uniqueWordCount == 0)
            return -1;

        if(!top100Generated) getTop100();


        return top100[position].getValue();
    }

    // helper function that gets the top 100 elements by count (used in getWordCountInListAt and getWordInListAt)
    public void getTop100() {
        
        Pair<String, Integer>[] arr = uniqueWordsTree.toSortedArray();
        
        int pos = Math.min(uniqueWordsTree.size(), 100);
        int current = 0;
        int indexOfMax = 0;
        top100 = (Pair<String, Integer>[]) new Pair[pos];
        String maxKey = "";
        Integer maxVal = -1;
        while (pos > 0) {
            maxVal = Integer.MIN_VALUE;
            for (int i = 0; i < arr.length; i++) {
                if (maxVal.compareTo(arr[i].getValue()) < 0) {
                    maxVal = arr[i].getValue();
                    maxKey = arr[i].getKey();
                    indexOfMax = i;
                }
            }
            top100[current] = new Pair(maxKey, maxVal);
            arr[indexOfMax].setvalue(-1);
            pos--;
            current++;
        }
        top100Generated = true;
    }
}
