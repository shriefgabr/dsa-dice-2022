package oy.tol.tira.books;


public final class BookFactory {
    private BookFactory() {
    }

    public static Book createBook() {
        Book theBook = new MyBookImplementation();
        return theBook;
    }
}
