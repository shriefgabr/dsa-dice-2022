package oy.tol.tira.books;

public class LinkedListImplementation<E> implements LinkedListInterface<E> {

   private class Node<T> {
      Node(T data) {
         element = data;
         next = null;
      }

      T element;
      Node<T> next;

      @Override
      public String toString() {
         return element.toString();
      }
   }

   private Node<E> head = null;
   private int size = 0;

   @Override
   public void add(E element) throws NullPointerException, LinkedListAllocationException {
      if (element == null)
         throw new NullPointerException("Cannot add null element");

      try {
         Node<E> newNode = new Node<E>(element);

         if (head == null) {
            head = newNode;
         } else {
            Node<E> currentNode = head;
            while (currentNode.next != null) {
               currentNode = currentNode.next;
            }
            currentNode.next = newNode;
         }

         size++;
      } catch (LinkedListAllocationException e) {
         throw new LinkedListAllocationException("Linked list allocation failed.");
      }
   }

   @Override
   public void add(int index, E element)
         throws NullPointerException, LinkedListAllocationException, IndexOutOfBoundsException {
      if (index < 0 || index > size)
         throw new IndexOutOfBoundsException();

      if (element == null)
         throw new NullPointerException();

      try {
         Node<E> newNode = new Node<E>(element);

         if (index == 0) {
            newNode.next = head;
            head = newNode;
         } else {
            Node<E> currentNode = head;
            for (int i = 0; i < index - 1; i++) {
               currentNode = currentNode.next;
            }
            newNode.next = currentNode.next;
            currentNode.next = newNode;
         }
         size++;
      } catch (LinkedListAllocationException e) {
         throw new LinkedListAllocationException("Linked list allocation failed.");
      }
   }

   @Override
   public boolean remove(E element) throws NullPointerException {
      if (element == null)
         throw new NullPointerException();
         
      if (size == 0)
         return false;
      Node<E> prevNode = new Node<E>(null);
      Node<E> currentNode = head;

      while (currentNode.next != null) {
         if (currentNode.element == element) {
            prevNode.next = currentNode.next;
            return true;
         }
         prevNode = head;
         currentNode = currentNode.next;
      }
      size--;
      return false;
   }

   @Override
   public E remove(int index) throws IndexOutOfBoundsException {

      if (index >= size || size == 0 || index < 0)
         throw new IndexOutOfBoundsException();

      Node<E> currentNode = head;
      if (index == 0) {
         head = head.next;
      } else {
         for (int i = 0; i < index - 1; i++) {

            currentNode = currentNode.next;
         }

         currentNode.next = currentNode.next.next;
      }

      size--;
      return null;
   }

   @Override
   public E get(int index) throws IndexOutOfBoundsException {
      if (index >= size || index < 0)
         throw new IndexOutOfBoundsException("Index is larger than list size.");

      if (this.head == null)
         return null;
      else {
         int counter = 0;
         Node<E> currentNode = head;

         while (currentNode != null && counter != index) {
            currentNode = currentNode.next;
            counter++;
         }
         return (E) currentNode.element;
      }
   }

   @Override
   public int indexOf(E element) throws NullPointerException {
      if (element == null)
         throw new NullPointerException("List is empty");
      if (size == 0)
         return -1;

      int index = 0;
      Node<E> currentNode = head;

      while (currentNode != null) {
         if (currentNode.element == element)
            return index;
         index++;
         currentNode = currentNode.next;
      }

      return -1;
   }

   @Override
   public int size() {
      return size;
   }

   @Override
   public void clear() {
      head = null;
      size = 0;

   }

   @Override
   public void reverse() {

      Node<E> nextNode = null;
      Node<E> prevNode = null;

      Node<E> currentNode = head;

      while (currentNode != null) {

         nextNode = currentNode.next;
         currentNode.next = prevNode;
         prevNode = currentNode;
         currentNode = nextNode;

      }

      head = prevNode;
   }

   @Override
   public String toString() {
      if (size == 0)
         return "[]";

      Node<E> currentNode = head;

      String retStr = "[" + currentNode.element;
      currentNode = currentNode.next;
      while (currentNode != null) {
         retStr += ", " + currentNode.element;
         currentNode = currentNode.next;
      }
      retStr += "]";

      return retStr;
   }

}
