package oy.tol.tira.books;

public class TreeNode<K extends Comparable<K>, V> {
    TreeNode<K,V> left = null;
    TreeNode<K,V> right = null;
    K key = null;
    V value = null;

    public int add(TreeNode<K,V> node, int depth) {
        depth++;
        if (node.key.compareTo(key) < 0) {
            if (left == null)
                left = node;
            else
                depth = left.add(node, depth);
        }

        else if (node.key.compareTo(key) > 0) {
            if (right == null)
                right = node;
            else
                depth = right.add(node, depth);
        }
        else {
            this.key = node.key;
            this.value = node.value;
        }
        return depth;
    }

}
