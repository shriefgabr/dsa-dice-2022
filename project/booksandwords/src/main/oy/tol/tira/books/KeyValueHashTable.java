package oy.tol.tira.books;

public class KeyValueHashTable<K extends Comparable<K>, V> implements Dictionary<K, V> {

    private LinkedListImplementation<Pair<K, V>>[] table = null;
    private int count = 0;
    private int collisionCounter = 0;
    static double FILL_FACTOR = 0.80;
    private int reallocationCounter = 0;

    public KeyValueHashTable(int capacity) throws OutOfMemoryError {
        try {
            ensureCapacity(capacity);
        } catch (OutOfMemoryError e) {
            throw new OutOfMemoryError("Cannot allocate an array. Out of memory.");
        }
    }

    public KeyValueHashTable() throws OutOfMemoryError {
        try {
            ensureCapacity(20);
        } catch (OutOfMemoryError e) {
            throw new OutOfMemoryError("Cannot allocate an array. Out of memory.");
        }
    }

    @Override
    public Type getType() {
        return Type.HASHTABLE;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void ensureCapacity(int size) throws OutOfMemoryError {
        try {
            if (size < 20) {
                size = 20;
            }
            table = (LinkedListImplementation<Pair<K, V>>[]) new LinkedListImplementation[size];
        } catch (OutOfMemoryError e) {
            throw new OutOfMemoryError("Cannot allocate an array. Out of memory.");
        }
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public String getStatus() {

        String toReturn = String.format(
                "Hash table fill factor is %f\nHash table had %d collisions when filling the hash table.\nHash table had to reallocate %d times. Everytime doubling its size.\nHash table current fill rate is %.2f%%%n",
                FILL_FACTOR, collisionCounter, reallocationCounter, (count / (double) table.length) * 100.0);

        return toReturn;
    }

    @Override
    public boolean add(K key, V value) throws IllegalArgumentException, OutOfMemoryError {
        if (null == key || value == null)
            throw new IllegalArgumentException("Neither key nor value can be null");

        try {
            Pair<K, V> p = new Pair<K, V>(key, value);
            if ((1.0 * count) / (1.0 * table.length) >= FILL_FACTOR) {
                reallocate();
            }
            int hashValue = key.hashCode();
            int index = (hashValue & 0x7fffffff) % table.length;

            if (table[index] == null) {
                table[index] = new LinkedListImplementation<Pair<K, V>>();
                table[index].add(p);
                count++;
            } else {
                collisionCounter++;
                table[index].add(p);
                count++;
            }

            return true;
        } catch (OutOfMemoryError e) {
            throw new OutOfMemoryError("Cannot reallocate hash table. Out of memory.");
        }

    }

    @Override
    public V find(K key) throws IllegalArgumentException {
        if (null == key)
            throw new IllegalArgumentException("Not key nor value can be null");
        int hashValue = key.hashCode();
        int index = (hashValue & 0x7fffffff) % table.length;

        if (table[index] != null) {
            for (int i = 0; i < table[index].size(); i++)
                if (key.equals(table[index].get(i).getKey()))
                    return table[index].get(i).getValue();
        }

        return null;
    }

    @Override
    @java.lang.SuppressWarnings({ "unchecked" })
    public Pair<K, V>[] toSortedArray() {
        Pair<K, V>[] sorted = (Pair<K, V>[]) new Pair[count];
        int newIndex = 0;
        for (int i = 0; i < table.length; i++) {
            if (table[i] != null) {
                for (int j = 0; j < table[i].size(); j++) {
                    sorted[newIndex++] = table[i].get(j);
                }
            }
        }
        Algorithms.fastSort(sorted, 0, sorted.length - 1);
        return sorted;
    }

    @Override
    public void compress() throws OutOfMemoryError {
        try {
            Algorithms.partitionByRule(table, table.length, element -> element == null);
        } catch (OutOfMemoryError e) {
            throw new OutOfMemoryError("Cannot compress array. Out of memory.");
        }
    }

    public void reallocate() {
        LinkedListImplementation<Pair<K, V>>[] temp = (LinkedListImplementation<Pair<K, V>>[]) new LinkedListImplementation[table.length
                * 2];
        collisionCounter = 0;
        for (int i = 0; i < table.length; i++) {
            if (table[i] != null) {
                for (int j = 0; j < table[i].size(); j++) {

                    K key = table[i].get(j).getKey();
                    V value = table[i].get(j).getValue();

                    int hashValue = key.hashCode();
                    int index = (hashValue & 0x7fffffff) % temp.length;
                    Pair<K, V> p = new Pair<K, V>(key, value);

                    if (temp[index] == null) {
                        temp[index] = new LinkedListImplementation<Pair<K, V>>();
                        temp[index].add(p);
                    } else {
                        collisionCounter++;
                        temp[index].add(p);
                    }
                }
            }
        }
        table = temp;
        reallocationCounter++;
    }

}
