package oy.tol.tra;

import java.util.function.Predicate;

public class Algorithms {

    public static <T> int partitionByRule(T[] array, int count, Predicate<T> rule) {

        int nextIndex = 0;
        int index = count;
        for (int i = 0; i < count; i++) {
            if (rule.test(array[i])) {
                index = i;
                break;
            }
        }

        if (index >= count) {
            return count;
        }

        nextIndex = index + 1;

        while (nextIndex < count) {
            if (!rule.test(array[nextIndex])) {
                swap(array, index, nextIndex);
                index = index + 1;
            }
            nextIndex++;
        }
        return index;

    }

    public static <T> void swap(T[] array, int first, int second) {

        T tmp = array[second];
        array[second] = array[first];
        array[first] = tmp;
    }

    public static class ModeSearchResult<T> {
        public T theMode;
        public int count = 0;
    }

    public static <T extends Comparable<T>> ModeSearchResult<T> findMode(T[] array) {
        ModeSearchResult<T> result = new ModeSearchResult<>();
        if (array == null) {
            result.theMode = null;
            result.count = -1;
            return result;
        }

        if (array.length < 2) {
            result.theMode = null;
            result.count = -1;
            return result;
        }

        if (array.length == 2 && (array[0] == array[1])) {
            result.theMode = array[0];
            result.count = 2;
            return result;
        }

        for (int i = 0; i < array.length; i++) {

            int localCount = 0;
            for (int j = 0; j < array.length; j++) {
                if (array[i].compareTo(array[j]) == 0) {
                    localCount++;
                }
            }

            if (localCount > result.count) {
                result.count = localCount;
                result.theMode = array[i];
            }
        }

        return result;
    }

    public static <T extends Comparable<T>> void sort(T[] array) {
        int n = 1;
        while (n < array.length) {
            int i = array.length - 1;
            while (i > 0) {
                if (array[i].compareTo(array[i - 1]) < 0) {
                    swap(array, i - 1, i);
                }
                i--;
            }
            n++;
        }
    }

    public static <T> void reverse(T[] array) {
        int i = 0;
        while (i < array.length / 2) {

            swap(array, i, array.length - i - 1);
            i++;
        }
    }

}
