# Learnings from the course tasks and reports

Write a couple of sentences per task, what did you learn from the task, how did you manage to do it.

If a task asks you to **report** something, do write the report in this document.

## 00-init

## 01-arrays

## 02-mode
Based on the code itself, the time complexity is O(n<sup>2</sup>) since it consists of a nested for loop. The memory complexity is O(1) since it just keeps track of the `localCount` variable and updates the `result.TheMode` and `result.count` based off of that.

### Graphs
#### Strings
![Performance - Strings](02-mode/Strings.png)
#### Doubles
![Performance - Doubles](02-mode/Doubles.png)
>**Comment** The time grows exponentially with the number of elements. This is expected since I used nested for loop making the time complexity O(n^2).\
>**Observation** Doubles were generally faster to process and find the mode in than Strings. 

## 03-draw


## 04-1-stack


## 04-2-queue


## 04-3-linkedlist


## 05-binsearch


## 05-invoices


## 67-phonebook

### **Overview**
#### **Hash Function**
Since we are dealing with strings for the keys (First name, last name), I used the hash function that iterates over the string characters and returns a numerical value that represents it.
```
public int hashCode() {
    int result = 1;
    int prime = 31;
    for(int i = 0; i < getFullName().length(); i++){
        result = prime*result + getFullName().charAt(i);
    }
}
```
The hashing function uses a prime number '31', multiplies it by result which takes an initial value of 1, then adds to it the numeric value of the current character. I tried replacing multiplication with bit shifting to optimize it but there was no noticeable difference.



#### **Fill Factor**
 Most resources online advised that a fill factor of 0.75 or 0.80 was used, so I tested both. Between 0.75 and 0.80 there was only a very negligible difference (~1%) that can depend on lots of other factors such as available computer resources at the time for instance.

#### **Growing the hash table**
In the hash table whenever a `FILL_FACTOR` of `0.80` was reached, the table contents were reallocated to a new table double its old size. All the indices are also recalculated after allocation.
#### **BST depth**
The max depth the BST went to allocate a new element is 35. I did not use a hash function for this purpose since the goal of the BST is to have distinct items.
#### **Handling collisions**
I implemented both methods to implement hash tables: chaining and probing. In the hash table of the project, I used probing. Later on in the BooksAndWords project I used chaining.

To implement probing, I allocated an array to hold the key-value pairs. When adding a new pair, I check to see if that array index holds any elements, if it is empty, the element is added. Else, a while loop searches for the next null index to add the element.

 To implement chaining, I utilized my linked list implementation from the course and defined an array that holds the root node of each linked list. If a collision occurs, the new key is add to the end of the list at that index. Index of the list was calculated by `hashCode() % table size`. This made sure that the index is between 0 and table size.

In case of a BST, if the key to be added is already in the tree, its value is replaced with the value.

I have not tried other techniques for BST as I did not see any need for that, but for hash table I attempted both probing and chaining. They both gave very similar results.

#### **Different methods**
Since I did not try more than one method in BST, and two in hash table. 

for the `getStatus()` I print:
- Hash table reallocation times and the resizing factor.
- Hash table fill factor
- Hash table fill rate
- Hash table number of collisions
- Hash table number of probing in the worst case (In case of probing) 
- Tree max depth reached when adding elements


These are all good factors to determine the performance of the algorithms. Reallocation times, fill factor, fill rate, and number of collisions show if the resizing factor needs to be decreased or increased to optimize time and memory.

Tree max depth shows if the tree is to deep and consequently unbalanced, making it take longer to add new elements. 


#### **Sorting algorithms**
I used `Algorithms.quickSort()`. I did not try other sorting algorithms as quickSort has a good enough time complexity `O(n*log(n))`. No modifications to heap or stack were made in this project.

#### **Execution time
![Performance of hash table](67-phonebook/HashTablePerformance.png)
![Performance of hash table](67-phonebook/HashTablePerformance-Aggregate.png)


## Optional tasks

### **BooksAndWords Project**
#### **Implementation Choices**
I used arrays, hash tables, and BSTs (HT and BST are my own implementation). To collect the ignore words, I used a hash table to store them. For the unique words, I used a BST since it contains only distinct keys and that is exactly what is needed.

#### **Correctness**

I believe my implementation is generally correct, the only ongoing issue is listed below:
- When running the Java code it says that `Book.createBook()` is not found. I am not sure at the moment why this happens, but will investigate it.

#### **Time complexity**
Each method has a different time complexity, I will discuss the most important ones below:
- `countUniqueWords()` This method reads all the words (O(n)), checks if the word should be ignored (O(1)), then inserts it to the binary search tree (O(log(n))). So the time complexity for it is `O(n*log(n))`.
- `getWordInListAt()` In the worst case (if top100 array is not ready), the time complexity for this is `O(n)`. If the array is ready, then the time complexity is `O(1).
- `getWordCountInListAt()` In the worst case (if top100 array is not ready), the time complexity for this is `O(n)`. If the array is ready, then the time complexity is `O(1)`.
- `getTop100()` This methods is a helper function that scans the entire `uniqueWordsTree` to get the top 100 elements (or less if the number of unique words is less). Its time complexity is `O(n*log(n))`.

>**Comment** 
The traversal method utilized in `toSortedArray()`  has a time complexity of `O(n)` since it accesses all the elements in the tree. `getTop100()` uses that array to find the top 100 by scanning the array for 100 times, everytime updating the max key-value pair and inserting it to the `top100` array.
  
#### **Difficulties**
There were not much difficulties except for tokenizing the array. I used a regular expression pattern to tokenize it in this case.
#### **Learnings**
The project was a good hands-on (along with the Phonebook project). I did not have much Java experience prior to starting the course so the projects solidified my understanding of the Algo-DS concepts as well as the Java syntax.
# Feedback section

General feedback and costructive development ideas for the course, please!